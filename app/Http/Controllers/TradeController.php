<?php

namespace App\Http\Controllers;

use App\History;
use App\Income;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Trade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerUserController as BaseVoyagerUserController;
use TCG\Voyager\Models\Setting;

class TradeController extends BaseVoyagerUserController
{
    public function updateTrade(Request $request)
    {

        if ($request->mode == 'close-trade') {
            $trade = Trade::whereDate('created_at', Carbon::today()->toDateString())->first();
            if ($trade) {

                $traded = Trade::find($trade->id);
                $traded->active = 0;
                if ($traded->save()) {

                    $setting = Setting::where('key', 'admin.trade');
                    $setting->update(['value' => 'close']);

                    echo json_encode(['status' => 'success']);
                }
            }
        } else {
            $count = Trade::whereDate('created_at', Carbon::today()->toDateString())->count();
            if ($count == 0) {
                $trade = new Trade();
                $trade->active = 1;
                if ($trade->save()) {

                    $setting = Setting::where('key', 'admin.trade');
                    $setting->update(['value' => 'open']);
                    echo json_encode(['status' => 'success']);
                }
            } else {
                echo json_encode(['status' => 'error']);
            }
        }
    }

    public function updateProfit(Request $request)
    {
        $trade = Trade::whereDate('created_at', Carbon::today()->toDateString())->first();
        if ($trade) {

            $trading = Trade::find($trade->id);
            $trading->profit = $request->profit_value;
            $trading->save();

            $incomes = Income::whereDate('created_at', Carbon::today()->toDateString())->get();

            if (sizeof($incomes) > 0) {
                //update
                $input = [];
                $ids = [];
                foreach ($incomes as $key => $item) {
                    $users = User::select('users.*', 'packages.title', 'packages.price')
                        ->join('packages', 'packages.id', '=', 'users.package_id')->role(['5'])
                        ->where('users.id', $item->user_id)->first();

                    $income = Income::find($item->id);
                    $income->income = $users->price * ($request->profit_value / 100);
                    $income->trade_id = $trading->id;
                    $income->package_id = $users->package_id;
                    $income->package_price = $users->price;
                    $income->save();
                }
            } else {
                //insert
                $users = User::select('users.*', 'packages.title', 'packages.price')
                    ->join('packages', 'packages.id', '=', 'users.package_id')->role(['5'])
                    ->where('users.transfer', 1)
                    ->get();
                $input = [];
                foreach ($users as $key => $user) {
                    $input[$key]['user_id'] = $user->id;
                    $input[$key]['income'] = $user->price * ($request->profit_value / 100);
                    $input[$key]['trade_id'] = $trading->id;
                    $input[$key]['package_id'] = $user->package_id;
                    $input[$key]['package_price'] = $user->price;
                    $input[$key]['created_at'] = now();
                    $input[$key]['updated_at'] = now();
                }
                Income::insert($input);
            }

            $allIncome = Income::whereDate('incomes.created_at', Carbon::today()->toDateString())
                ->join('users', 'users.id', '=', 'incomes.user_id')
                ->join('packages', 'packages.id', '=', 'users.package_id')
                ->where('users.role_id', 5)->where('users.transfer', 1)
                ->get();
            // Fetch all records
            $userData['data'] = $allIncome;

            echo json_encode($userData);
        } else {
            echo json_encode(['status' => 'error']);
        }

    }

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object)['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('trades.*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            //member only
            if (Auth::user()->role_id === 5) {
                $query = $query->join('incomes', 'incomes.trade_id', '=', 'trades.id')
                    ->where('incomes.user_id', Auth::user()->id);
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest("trades." . $model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        $allIncome = Income::where('incomes.trade_id',$id)
            ->join('users', 'users.id', '=', 'incomes.user_id')
            ->join('packages', 'packages.id', '=', 'users.package_id')
            ->where('users.role_id', 5)->where('users.transfer',1);
        if (Auth::user()->role->id === 1 || Auth::user()->role->id === 4) {
            $allIncome = $allIncome->get();
            $withdraw = null;
        } else if (Auth::user()->role->id === 5) {
            $allIncome = $allIncome->where('user_id', Auth::user()->id)->first();

            $income = Income::where('user_id',Auth::user()->id)->get();
            $history = History::where('user_id',Auth::user()->id)->where('status',1)->get();
            $withdraw = $income->sum->income - $history->sum->amount;
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'allIncome','withdraw'));
    }
}
