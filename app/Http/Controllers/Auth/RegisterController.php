<?php

namespace App\Http\Controllers\Auth;

use App\Bank;
use App\Http\Controllers\Controller;
use App\Package;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UploadController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'package_id' => ['required'],
            'bank_id' => ['required'],
            'bank_branch' => ['required'],
            'book_bank_code' => ['required'],
            'book_bank_name' => ['required'],
            'avatar' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'package_id' => $data['package_id'],
            'role_id' => 5,
            'bank_id' => $data['bank_id'],
            'bank_branch' => $data['bank_branch'],
            'book_bank_code' => $data['book_bank_code'],
            'book_bank_name' => $data['book_bank_name'],
            'avatar' => $data['avatar']
        ]);
    }

    public function showRegistrationForm()
    {
        $packets = Package::all();
        $banks = Bank::all();
        return view('auth.register', compact('packets', 'banks'));
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $upload =  new UploadController;
        $avatar = $upload->uploadAvatar($request);

        $data = $request->all();
        $data['avatar'] = $avatar;

        event(new Registered($user = $this->create($data)));

        //return redirect()->route('login')->with('success', 'สมัครสำเร็จ!');

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 201)
            : redirect($this->redirectPath());
    }
}
