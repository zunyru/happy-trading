<?php

namespace App\Http\Controllers\Voyager;

use App\Banner;
use App\History;
use App\Income;
use App\Package;
use App\Trade;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {
        $trade = Trade::whereDate('created_at', Carbon::today()->toDateString())->first();
        $banners = Banner::where('active','on')->get();
        $packet = Package::find(Auth::user()->package_id);

        $incomes = Income::whereDate('created_at', Carbon::today()->toDateString())->get();
        if(sizeof($incomes) > 0){
            $allIncome = Income::whereDate('incomes.created_at', Carbon::today()->toDateString())
                ->join('users', 'users.id', '=', 'incomes.user_id')
                ->join('packages', 'packages.id', '=', 'users.package_id')
                ->where('users.role_id',5);

        }else{
            $allIncome = User::select('users.*', 'packages.title','packages.price')
                ->join('packages', 'packages.id', '=', 'users.package_id')->role(['5']);
        }
        $withdraw = null;
        if(Auth::user()->role->id === 1 || Auth::user()->role->id === 4){
            $allIncome = $allIncome->get();

            $proposal = History::where('status',0)->first();

        }else if(Auth::user()->role->id === 5){
            $allIncome = $allIncome->where('users.id',Auth::user()->id)->first();

            //get withdraw
            //SELECT (SELECT SUM(income) as Qty1  FROM incomes WHERE user_id = 9) - (SELECT SUM(amount) as Qty2 FROM histories WHERE user_id = 9) AS `sumf`
           $income = Income::where('user_id',Auth::user()->id)->get();
           $history = History::where('user_id',Auth::user()->id)->whereIn('status',[1,0])->get();
           $withdraw = $income->sum->income - $history->sum->amount;

           $proposal = null;

           $status = History::where('status',0)->where('user_id',Auth::user()->id)->first();
           if(!$status) {
               $proposal = History::where('status', 1)->where('user_id', Auth::user()->id)
                   ->whereNull('viewer')
                   ->first();
           }
        }

        return Voyager::view('voyager::index',compact('proposal','packet','banners','trade','allIncome','withdraw'));
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
