<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

class Member extends Model
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider', 'email', 'name', 'password', 'picture'];

}
