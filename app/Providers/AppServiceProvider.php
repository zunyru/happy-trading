<?php

namespace App\Providers;

use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;
use App\FormFields\SummernoteFormField;
use App\FormFields\SlugFormField;
use App\FormFields\MyMultiSelect;
use App\FormFields\FileuploaderMultiImage;
use App\FormFields\DateTime;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(SummernoteFormField::class);
        Voyager::addFormField(SlugFormField::class);
        Voyager::addFormField(MyMultiSelect::class);
        Voyager::addFormField(DateTime::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
