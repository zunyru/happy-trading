<?php

use Illuminate\Support\Facades\Route;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect()->route('login');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //summernote
    Route::post('summernote', 'Formfield\SummernoteController@upload');

    //memeber
    Route::get('/members/bank', 'MemberController@bankedit')->name('voyager.member.bank');

    Route::get('/members', 'MemberController@index')->name('voyager.member.index');
    Route::get('/members/create', 'MemberController@create')->name('voyager.member.create');
    Route::get('/members/{id}/edit', 'MemberController@edit')->name('voyager.member.edit');
    Route::get('/members/{id}', 'Voyager\VoyagerUserController@show')->name('voyager.member.show');
    Route::delete('/members/{$id}', 'MemberController@destroy')->name('voyager.member.destroy');
    Route::put('/members/{id}', 'MemberController@update')->name('voyager.member.update');


    //Dividend
    Route::get('/dividens', 'DividendController@indexdividend')->name('voyager.dividend.index');
    Route::get('/dividens/{id}', 'DividendController@show')->name('voyager.dividend.show');
    Route::delete('/dividens/{id}','DividendController@destroy')->name('voyager.dividend.destroy');

    Route::get('/withdraw', 'DividendController@index')->name('voyager.withdraw.index');

    Route::get('/histories', 'HistoryController@index')->name('voyager.histories.index');
    Route::get('/histories/{id}', 'HistoryController@show')->name('voyager.histories.show');
    Route::delete('/histories/{id}','DividendController@destroy')->name('voyager.histories.destroy');
    Route::get('/histories/create', 'DividendController@create')->name('voyager.histories.create');
    Route::post('/histories', 'DividendController@store')->name('voyager.histories.store');

    //Trade
    Route::post('/update-trade','TradeController@updateTrade')->name('update-trade');
    Route::post('/update-profit','TradeController@updateProfit')->name('update-profit');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
