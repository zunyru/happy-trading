@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">

                <div class="h2 text-center mb-4">{{ __('Register') }}</div>

                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="book_bank_name"
                               class="col-md-4 col-form-label text-md-right">{{ 'รูปโปรไฟล์' }}</label>

                        <div class="col-md-6">
                            <input type="file" name="avatar" class="files" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ 'ชื่อ-นามสกุล' }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email"
                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" required
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm"
                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" required
                                   name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm"
                               class="col-md-4 col-form-label text-md-right">{{ 'แพ็กเกจ ' }}</label>

                        <div class="col-md-6">
                            <select class="form-control" name="package_id" required>
                                @foreach($packets as $packet)
                                    <option
                                        value="{{ $packet->id }}">{{ $packet->title." (".number_format($packet->price,0)." ".$packet->currency.")" }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <h4 class="text-center py-3">ข้อมูลธนาคาร</h4>
                    <div class="form-group row">
                        <label for="password-confirm"
                               class="col-md-4 col-form-label text-md-right">{{ 'ธนาคาร ' }}</label>

                        <div class="col-md-6">
                            <select class="form-control vodiapicker" name="bank_id" required>
                                <option value="">{{ 'กรุณเลือกธนาคาร' }}</option>
                                @foreach($banks as $bank)
                                    <option data-thumbnail="{{ Voyager::image($bank->logo) }}});"
                                            value="{{ $bank->id }}">{{ $bank->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="book_bank_code"
                               class="col-md-4 col-form-label text-md-right">{{ 'สาขาธนาคาร' }}</label>

                        <div class="col-md-6">
                            <input id="bank_branch" type="text"
                                   class="form-control @error('bank_branch') is-invalid @enderror"
                                   name="bank_branch" value="{{ old('bank_branch') }}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="book_bank_code"
                               class="col-md-4 col-form-label text-md-right">{{ 'เลขที่บัญชี' }}</label>

                        <div class="col-md-6">
                            <input id="book_bank_code" type="text"
                                   class="form-control @error('book_bank_code') is-invalid @enderror"
                                   name="book_bank_code" value="{{ old('book_bank_code') }}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="book_bank_name"
                               class="col-md-4 col-form-label text-md-right">{{ 'ชื่อบัญชี' }}</label>

                        <div class="col-md-6">
                            <input id="book_bank_name" type="text"
                                   class="form-control @error('book_bank_name') is-invalid @enderror"
                                   name="book_bank_name" value="{{ old('book_bank_name') }}" required autofocus>
                        </div>
                    </div>
                   {{-- <div class="form-group row">
                        <label for="book_bank_name"
                               class="col-md-4 col-form-label text-md-right">{{ 'หลักฐานรูปหน้าบัญชี' }}</label>

                        <div class="col-md-6">
                            <input type="file" name="bookbank_att" class="files" required>
                        </div>
                    </div>--}}

                    <br>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-md btn-block">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/fileuploader/dist/jquery.fileuploader.min.js') }}" type="text/javascript"></script>
    <script>

        $('input.files').fileuploader({
            maxSize: 7,
            fileMaxSize: 7,
            extensions: ['jpg', 'jpeg', 'png'],
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<div class="fileuploader-icon-main"></div>' +
                '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                '<p>${captions.or}</p>' +
                '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 1,
        });

    </script>
@endpush
