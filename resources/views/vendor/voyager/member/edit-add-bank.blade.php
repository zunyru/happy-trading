@extends('voyager::master')
@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).'
'.$dataType->getTranslatedAttribute('display_name_singular'))
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- font -->
    <link href="{{ asset('assets/fileuploader/dist/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
    <!-- css -->
    <link href="{{ asset('assets/fileuploader/dist/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
    <link rel="stylesheet"
          href="{{ asset('assets/fileuploader/drag-drop/css/jquery.fileuploader-theme-dragdrop.css') }}">
    <style>
        .fileuploader-theme-dragdrop .fileuploader-input {
            padding: unset;
        }
    </style>
    <!-- Add fancyBox main JS and CSS files -->
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" media="screen"/>
@stop
@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop
@section('content')
    <div class="page-content container-fluid user">
        <form class="form-edit-add" role="form"
              action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.member.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="custom" value="member">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}
            <h4 style="padding-left: 10px">ข้อมูลธุรกรรมทางการเงิน</h4>
            <div class="row hidden">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                        {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="panel-body">
                            <div class="form-group">
                                <input type="hidden" name="package_id" value="{{ $dataTypeContent->package_id }}">
                                <input type="hidden" name="transfer" value="{{ $dataTypeContent->transfer }}">
                                <label for="name">{{ 'ชื่อ-นามสกุล' }}</label>
                                <input type="hidden" class="form-control" id="name" name="name"
                                       placeholder="{{ 'ชื่อ-นามสกุล'}}"
                                       value="{{ old('name', $dataTypeContent->name ?? '') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('voyager::generic.email') }}</label>
                                <input type="hidden" class="form-control" id="email" name="email"
                                       placeholder="{{ __('voyager::generic.email') }}"
                                       value="{{ old('email', $dataTypeContent->email ?? '') }}">
                            </div>
                            <input type="hidden" name="role_id" value="5">
                            @php
                                if (isset($dataTypeContent->package_id)) {
                                $selected_package = $dataTypeContent->package_id;
                                } else {
                                $selected_package = "";
                                }
                            @endphp
                            <div class="form-group">
                                <label for="locale">{{ 'แพ็กเกจ' }}</label>
                                <input type="hidden" value="package" value="{{ $selected_package }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body">
                            <div class="form-group">
                                @if(isset($dataTypeContent->avatar))
                                    <img
                                        src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}"
                                        style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                                @endif
                                <input type="file" data-name="avatar" name="avatar" class="files-fileuploader">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::user()->role->id === 5)
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="bookbank_att">{{ 'รูปหน้าบัตรธนาคาร' }}</label>
                                    @if(isset($dataTypeContent->bookbank_att))
                                        <img
                                            src="{{ filter_var($dataTypeContent->bookbank_att, FILTER_VALIDATE_URL) ? $dataTypeContent->bookbank_att : Voyager::image( $dataTypeContent->bookbank_att ) }}"
                                            style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                                    @endif
                                    <input type="file" data-name="bookbank_att" name="bookbank_att"
                                           class="files-fileuploader">
                                </div>
                                <div class="form-group">
                                    @php
                                        if (isset($dataTypeContent->bank_id)) {
                                        $selected_bank = $dataTypeContent->bank_id;
                                        } else {
                                        $selected_bank = "";
                                        }
                                    @endphp
                                    <label for="bank_id">{{ 'ธนาคาร' }}</label>
                                    <select class="form-control select2" id="bank_id" name="bank_id" required>
                                        <option value="" selected>เลือกธนาคาร</option>
                                        @foreach ($banks as $bank)
                                            <option
                                                value="{{ $bank->id }}" {{ ($bank->id === $selected_bank ? 'selected' : '') }}>
                                                {{ $bank->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bank_branch">{{ 'สาขาธนาคาร' }}</label>
                                    <input type="text" class="form-control" id="bank_branch" name="bank_branch"
                                           placeholder="{{ 'สาขาธนาคาร' }}" required
                                           value="{{ old('bank_branch', $dataTypeContent->bank_branch ?? '') }}">
                                </div>
                                <div class="form-group">
                                    <label for="book_bank_code">{{ 'เลขที่บัญชี' }}</label>
                                    <input type="text" class="form-control" id="book_bank_code" name="book_bank_code"
                                           placeholder="{{ 'เลขที่บัญชี' }}" required
                                           value="{{ old('book_bank_code', $dataTypeContent->book_bank_code ?? '') }}">
                                </div>
                                <div class="form-group">
                                    <label for="book_bank_name">{{ 'ชื่อบัญชี' }}</label>
                                    <input type="text" class="form-control" id="book_bank_name" name="book_bank_name"
                                           placeholder="{{ 'ชื่อบัญชี' }}" required
                                           value="{{ old('book_bank_name', $dataTypeContent->book_bank_name ?? '') }}">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-body">
                                <h4 style="padding-left: 10px">ข้อมูลสำคัญ</h4>
                                <div class="form-group">
                                    <label for="bookbank_att">{{ 'รูปหน้าบัตรประชาชน' }}</label>
                                    @if(isset($dataTypeContent->crad_id))
                                        <img
                                            src="{{ filter_var($dataTypeContent->crad_id, FILTER_VALIDATE_URL) ? $dataTypeContent->crad_id : Voyager::image( $dataTypeContent->crad_id ) }}"
                                            style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                                    @endif
                                    <input type="file" data-name="crad_id" name="crad_id" class="files-fileuploader">
                                </div>
                                <div class="form-group">
                                    <label for="line">{{ 'รหัส USDT' }}</label>
                                    <input type="text" class="form-control" id="usdt" name="usdt"
                                           placeholder="{{ 'USDT' }}"
                                           value="{{ old('usdt', $dataTypeContent->usdt ?? '') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>
        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop
@section('javascript')
    <!-- js -->
    <script src="{{ asset('assets/fileuploader/dist/jquery.fileuploader.min.js') }}" type="text/javascript"></script>
    {{-- fancybox --}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js">
    </script>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });

        $(document).ready(function () {
            //fileuploader
            $('input.files-fileuploader').fileuploader({
                changeInput: '<div class="fileuploader-input">' +
                    '<div class="fileuploader-input-inner">' +
                    '<div class="fileuploader-icon-main"></div>' +
                    '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                    '<p>${captions.or}</p>' +
                    '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                    '</div>' +
                    '</div>',
                theme: 'dragdrop',
                maxSize: 7,
                fileMaxSize: 7,
                extensions: ['image/*'],
                limit: 1,
                quality: 80,
            });
            $('input.gallery_media').fileuploader({
                limit: 12,
                maxSize: 20,
                addMore: true,
                sorter: {
                    selectorExclude: null,
                    placeholder: null,
                    scrollContainer: window,
                    onSort: function (list, listEl, parentEl, newInputEl, inputEl) {
                        // onSort callback
                    }
                }
            });
            //fancybox
            $(".fancybox").fancybox();
        });
    </script>
@stop
