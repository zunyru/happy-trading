@extends('voyager::master')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.css') }}">
@stop
@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        @include('voyager::dimmers')
    </div>
    <div class="page-content">

        @if(sizeof($banners) > 0)
            <div class="page-content browse container-fluid">
                <div class="trading-page panel panel-bordered">
                    <div class="panel-body">
                        <div class="owl-carousel">
                            @foreach($banners as $banner)
                                <div>
                                    <img src="{{ Voyager::image($banner->image) }}" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(Auth::user()->role->id !== 5)
            @if(!empty($proposal))
                <div class="row">
                    <div class="page-content browse container-fluid">
                        <div class="col-md-12 col-sm-12 col-xs-auto">
                            <div class="alert alert-warning">
                                <strong>แจ้ง! </strong> มีการขอถอนเงินจากสมาชิก
                                <a href="{{ route('voyager.histories.index') }}"
                                   class="btn btn-link">ดูรายละเอียดที่นี้</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @else
            @if(!empty($proposal))
                <div class="row">
                    <div class="page-content browse container-fluid">
                        <div class="col-md-12 col-sm-12 col-xs-auto">
                            <div class="alert alert-success">
                                <strong>แจ้ง! </strong> Admin ได้ทำการอนุมัติการถอนเงินของท่านเรียบร้อยแล้ว
                                <a href="{{ route('voyager.histories.show',Auth::user()->id) }}" class="btn btn-link">ดูรายละเอียดที่นี้</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        <div class="page-content browse container-fluid">
            <div class="trading-page panel panel-bordered" style="background-color: #13131b;">
                <div class="panel-body">
                    <div class="row">
                        <div class="row justify-content-md-center">
                            <div class="col-md-4 col-sm-4 col-xs-auto"></div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                @if(Auth::user()->role->id === 1 || Auth::user()->role->id === 4)
                                    <h2 for="create_model text-center">ตั้งค่าการเปิด/ปิด Trade</h2>
                                    <ul class="radio">
                                        <li>
                                            <input type="radio" id="trade-open" name="trading"
                                                   value="open" @if(setting('admin.trade') === 'open') checked @endif>
                                            <label for="trade-open">{{ 'เปิด Trade' }}</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                        <li>
                                            <input type="radio" id="trade-close" name="trading"
                                                   value="close" @if(setting('admin.trade') === 'close') checked @endif>
                                            <label for="trade-close">{{ 'ปิด Trade' }}</label>
                                            <div class="check"></div>
                                        </li>
                                        </li>
                                    </ul>
                                @else
                                    <h1 for="create_model" class="text-happy text-center">
                                        วันนี้ {{ (setting('admin.trade') === 'open') ? 'กำลัง Trade' : 'ปิด Trade' }}
                                    </h1>
                                @endif
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-auto">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::user()->role->id === 5 && Auth::user()->transfer === 1)
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-auto">
                    <div class="page-content browse container-fluid">
                        <div class="trading-page panel panel-bordered" style="background-color: #13131b;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="">
                                        <h4 class="text-center">แพ็กเกจของคุณ</h4>
                                        <h3 class="text-center">
                                            <span class="text-happy">{{ $packet->title ?? "ยังไม่มีแพ็คเกจ" }}</span>
                                            @if(isset($packet->title))
                                                <span
                                                    class="text-happy">{{ "(".number_format($packet->price,0)." ".$packet->currency.")" }}</span>
                                            @endif
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-auto">
                    <div class="page-content browse container-fluid">
                        <div class="trading-page panel panel-bordered" style="background-color: #13131b;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="">
                                        <h4 class="text-center">ปันผลวันนี้</h4>
                                        @if(!empty($allIncome->price))
                                            <h3 class="text-center text-happy">
                                                {{ !empty($allIncome->income) ? number_format($allIncome->income,0)." THB" :  "ยังไม่มียอด" }}
                                            </h3>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-auto">
                    <div class="page-content browse container-fluid">
                        <div class="trading-page panel panel-bordered" style="background-color: #13131b;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="text-center">
                                        <h4 class="">ยอดเงินที่คุณถอนได้</h4>
                                        <small class="text-danger">ถอนขั้นต่ำ 300 บาท</small>
                                        <h3 class="text-center text-happy">
                                            {{ number_format($withdraw,0).' THB'}}
                                        </h3>
                                        @if($withdraw >= 300)
                                            <a href="{{ route('voyager.histories.show',Auth::user()->id) }}"
                                               class="btn btn-warning">ถอนเงิน</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @elseif(Auth::user()->role->id === 5)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-auto">
                    <div class="page-content browse container-fluid">
                        <div class="trading-page panel panel-bordered" style="background-color: #13131b;">
                            <div class="panel-body">
                                <h4 class="text-center">คุณยังไม่ได้ทำการโอนเงิน</h4>
                                <h3 class="text-center text-happy">
                                    หากคุณโอนเงินแล้วกรุณาติดต่อเจ้าหน้าที่เพื่อเปลี่ยนแปลงสถานะ
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        {{--   Admin     --}}
        @if(Auth::user()->role->id === 1 || Auth::user()->role->id === 4)
            @if($trade)
                <div class="row" id="trading">
                    <div class="col-md-12 col-sm-12 col-xs-auto">
                        <div class="page-content browse container-fluid">
                            <div class="trading-page panel panel-bordered">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-auto">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-auto">
                                            @if(setting('admin.trade') === 'close')
                                                <h4>ระบุกำไรที่ได้จากการ trage วันนี้ {{ DateThai(now()) }}</h4>
                                                <small class="text-danger">การเปลี่ยนแปลง
                                                    กำไรนี้จะมีผลกับสมาชิกทุกคน</small>
                                                <br>
                                                <select name="profit" id="profit" class="form-control select2">
                                                    <option value="">เลือกกำไรที่ได้จากการ Trade</option>
                                                    <option
                                                        value="0" {{ $trade->profit == '0' ? 'selected' : '' }} >
                                                        0%
                                                    </option>
                                                    <option
                                                        value="0.1" {{ $trade->profit == '0.1' ? 'selected' : '' }} >
                                                        0.1%
                                                    </option>
                                                    <option
                                                        value="0.2" {{ $trade->profit == '0.2' ? 'selected' : '' }} >
                                                        0.2%
                                                    </option>
                                                    <option
                                                        value="0.3" {{ $trade->profit == '0.3' ? 'selected' : '' }} >
                                                        0.3%
                                                    </option>
                                                    <option
                                                        value="0.4" {{ $trade->profit == '0.4' ? 'selected' : '' }} >
                                                        0.4%
                                                    </option>
                                                    <option
                                                        value="0.5" {{ $trade->profit == '0.5' ? 'selected' : '' }} >
                                                        0.5%
                                                    </option>
                                                    <option
                                                        value="0.6" {{ $trade->profit == '0.6' ? 'selected' : '' }} >
                                                        0.6%
                                                    </option>
                                                    <option
                                                        value="0.7" {{ $trade->profit == '0.7' ? 'selected' : '' }} >
                                                        0.7%
                                                    </option>
                                                    <option
                                                        value="0.8" {{ $trade->profit == '0.8' ? 'selected' : '' }} >
                                                        0.8%
                                                    </option>
                                                    <option
                                                        value="0.9" {{ $trade->profit == '0.9' ? 'selected' : '' }} >
                                                        0.9%
                                                    </option>
                                                    <option
                                                        value="1.0" {{ $trade->profit == '1.0' ? 'selected' : '' }} >
                                                        1.0%
                                                    </option>
                                                    <option
                                                        value="1.1" {{ $trade->profit == '1.1' ? 'selected' : '' }} >
                                                        1.1%
                                                    </option>
                                                    <option
                                                        value="1.2" {{ $trade->profit == '1.2' ? 'selected' : '' }} >
                                                        1.2%
                                                    </option>
                                                    <option
                                                        value="1.3" {{ $trade->profit == '1.3' ? 'selected' : '' }} >
                                                        1.3%
                                                    </option>
                                                    <option
                                                        value="1.4" {{ $trade->profit == '1.4' ? 'selected' : '' }} >
                                                        1.4%
                                                    </option>
                                                    <option
                                                        value="1.5" {{ $trade->profit == '1.5' ? 'selected' : '' }} >
                                                        1.5%
                                                    </option>
                                                    <option
                                                        value="1.6" {{ $trade->profit == '1.6' ? 'selected' : '' }} >
                                                        1.6%
                                                    </option>
                                                    <option
                                                        value="1.7" {{ $trade->profit == '1.7' ? 'selected' : '' }} >
                                                        1.7%
                                                    </option>
                                                    <option
                                                        value="1.8" {{ $trade->profit == '1.8' ? 'selected' : '' }} >
                                                        1.8%
                                                    </option>
                                                    <option
                                                        value="1.9" {{ $trade->profit == '1.9' ? 'selected' : '' }} >
                                                        1.9%
                                                    </option>
                                                    <option
                                                        value="2.0" {{ $trade->profit == '2.0' ? 'selected' : '' }} >
                                                        2.0%
                                                    </option>
                                                </select>
                                            @endif
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-auto">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table table-bordered" id="userTable">
                                            <thead>
                                            <tr>
                                                <th>ชื่อ-นามสกุล</th>
                                                <th>แพ็กเกจ</th>
                                                <th>เงินปันผลวันนี้</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($allIncome as $item)
                                                <tr>
                                                    <td align='left'>
                                                        {{ $item->name }}
                                                    </td>
                                                    <td align='center'>{{ $item->title." (".number_format($item->price,0).")" }}</td>
                                                    <td align='center'>{{ !empty($item->income) ? number_format($item->income,0) : "ยังไม่มียอด" }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        {{--    End Admin    --}}
    </div>
@stop

@section('javascript')
    <script src="{{ asset('assets/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".owl-carousel").owlCarousel({
                items: 2,
                loop: true,
                margin: 10,
                merge: true,
                stagePadding: 50,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        stagePadding: 20,
                        items: 1,
                        mergeFit: true
                    },
                    678: {
                        mergeFit: true
                    },
                    1000: {
                        mergeFit: false
                    }
                }
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $("input[name='trading']").change(function () {
            if ($("#trade-open").is(':checked')) {
                Swal.fire({
                    title: 'คุณแน่ใจที่จะเปิด Trade ใช่ไม ?',
                    text: "การเปิด trade สามารถทำได้ 1 ครั้งต่อวันเท่านั้น!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ใช่ , เปิด trade',
                    cancelButtonText: 'ยกเลิก',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: `{{ route('update-trade') }}`,
                            type: "POST",
                            dataType: 'json',
                            data: {
                                mode: "open-trade"
                            },
                            success: function (data, textStatus, jqXHR) {

                                if (data.status === 'success') {
                                    Swal.fire(
                                        'เปิดการ trade แล้ว!',
                                        'คุณได้ทำการเปิด Trade ของวันนี้แล้ว',
                                        'success'
                                    ).then(function () {
                                        window.location = "/";
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'มีการ Trade ไปแล้วในวันนี้!',
                                        text: 'ไม่สามารถเปิด Trade ได้แล้วในวันนี้...กรุณาเปิด trde ในวันต่อไป',
                                    });
                                    $("#trade-open").prop("checked", false);
                                    $("#trade-close").prop("checked", true);
                                    result.dismiss === Swal.DismissReason.cancel
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'เกิดข้อมผิดพลาดบางอย่าง...',
                                    text: 'กรุณาติดต่อ admin ที่ดูแลระบบ !',
                                });
                            }
                        });

                    } else {
                        $("#trade-open").prop("checked", false);
                        $("#trade-close").prop("checked", true);
                        result.dismiss === Swal.DismissReason.cancel
                    }
                })
            }
            if ($("#trade-close").is(':checked')) {
                Swal.fire({
                    title: 'คุณแน่ใจที่จะปิด Trade ใช่ไม ?',
                    text: "การปิด trade จะไม่สามารถเปิดได้อีกครั้งของวันนี้!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ใช่ , ปิด trade',
                    cancelButtonText: 'ยกเลิก',
                }).then((result) => {

                    if (result.value) {
                        $.ajax({
                            url: `{{ route('update-trade') }}`,
                            type: "POST",
                            dataType: 'json',
                            data: {
                                mode: "close-trade"
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (data.status === 'success') {
                                    Swal.fire(
                                        'ปิดการ trade แล้ว!',
                                        'คุณได้ทำการปิด Trade ของวันนี้แล้ว',
                                        'success'
                                    ).then(function () {
                                        window.location = "/";
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'มีการ Trade ไปแล้วในวันนี้!',
                                        text: 'ไม่สามารถเปิด Trade ได้แล้วในวันนี้...กรุณาเปิด trde ในวันต่อไป',
                                    });
                                    $("#trade-open").prop("checked", true);
                                    $("#trade-close").prop("checked", false);
                                    result.dismiss === Swal.DismissReason.cancel
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'เกิดข้อมผิดพลาดบางอย่าง...',
                                    text: 'กรุณาติดต่อ admin ที่ดูแลระบบ !',
                                });
                            }
                        });
                    } else {
                        $("#trade-open").prop("checked", true);
                        $("#trade-close").prop("checked", false);
                        result.dismiss === Swal.DismissReason.cancel
                    }
                })
            }
        });

        $("#profit").on("change", function (e) {
            if ($(this).val() != '') {
                $.ajax({
                    url: `{{ route('update-profit') }}`,
                    type: "POST",
                    dataType: "json",
                    data: {
                        profit_value: $(this).val()
                    },
                    success: function (response, textStatus, jqXHR) {
                        toastr.success("ดำเนินการเพิ่มเงินปันผลการ trade วันนี้แล้ว");
                        var len = 0;
                        $('#userTable tbody').empty(); // Empty <tbody>
                        if (response['data'] != null) {
                            len = response['data'].length;
                        }
                        if (len > 0) {
                            for (var i = 0; i < len; i++) {

                                var use_id = response['data'][i].user_id;
                                var name = response['data'][i].name;
                                var packet_name = response['data'][i].title;
                                var packet_price = response['data'][i].price;
                                var income = response['data'][i].income;

                                var tr_str = "<tr>" +
                                    "<td align='left'>" + name + "</td>" +
                                    "<td align='center'>" + packet_name + " (" + addCommas(packet_price) + ") " + "</td>" +
                                    "<td align='center'>" + addCommas(income) + "</td>" +
                                    "</tr>";

                                $("#userTable tbody").append(tr_str);
                            }
                        } else {
                            var tr_str = "<tr>" +
                                "<td align='center' colspan='4'>No record found.</td>" +
                                "</tr>";

                            $("#userTable tbody").append(tr_str);
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                        Swal.fire({
                            icon: 'error',
                            title: 'เกิดข้อมผิดพลาดบางอย่าง...',
                            text: 'กรุณาติดต่อ admin ที่ดูแลระบบ !',
                        });
                    }
                });
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: 'กรุณาเลือกกำไร',
                    text: 'เลือกกำไรของการ trade วันนี้',
                });
            }
        });

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

    </script>

    @if(isset($google_analytics_client_id) && !empty($google_analytics_client_id))
        <script>
            (function (w, d, s, g, js, fs) {
                g = w.gapi || (w.gapi = {});
                g.analytics = {
                    q: [], ready: function (f) {
                        this.q.push(f);
                    }
                };
                js = d.createElement(s);
                fs = d.getElementsByTagName(s)[0];
                js.src = 'https://apis.google.com/js/platform.js';
                fs.parentNode.insertBefore(js, fs);
                js.onload = function () {
                    g.load('analytics');
                };
            }(window, document, 'script'));
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script>
            // View Selector 2 JS
            !function (e) {
                function t(r) {
                    if (i[r]) return i[r].exports;
                    var o = i[r] = {exports: {}, id: r, loaded: !1};
                    return e[r].call(o.exports, o, o.exports, t), o.loaded = !0, o.exports
                }

                var i = {};
                return t.m = e, t.c = i, t.p = "", t(0)
            }([function (e, t, i) {
                "use strict";

                function r(e) {
                    return e && e.__esModule ? e : {"default": e}
                }

                var o = i(1), s = r(o);
                gapi.analytics.ready(function () {
                    function e(e, t, i) {
                        e.innerHTML = t.map(function (e) {
                            var t = e.id == i ? "selected " : " ";
                            return "<option " + t + 'value="' + e.id + '">' + e.name + "</option>"
                        }).join("")
                    }

                    function t(e) {
                        return e.ids || e.viewId ? {
                            prop: "viewId",
                            value: e.viewId || e.ids && e.ids.replace(/^ga:/, "")
                        } : e.propertyId ? {prop: "propertyId", value: e.propertyId} : e.accountId ? {
                            prop: "accountId",
                            value: e.accountId
                        } : void 0
                    }

                    gapi.analytics.createComponent("ViewSelector2", {
                        execute: function () {
                            return this.setup_(function () {
                                this.updateAccounts_(), this.changed_ && (this.render_(), this.onChange_())
                            }.bind(this)), this
                        },
                        set: function (e) {
                            if (!!e.ids + !!e.viewId + !!e.propertyId + !!e.accountId > 1) throw new Error('You cannot specify more than one of the following options: "ids", "viewId", "accountId", "propertyId"');
                            if (e.container && this.container) throw new Error("You cannot change containers once a view selector has been rendered on the page.");
                            var t = this.get();
                            return (t.ids != e.ids || t.viewId != e.viewId || t.propertyId != e.propertyId || t.accountId != e.accountId) && (t.ids = null, t.viewId = null, t.propertyId = null, t.accountId = null), gapi.analytics.Component.prototype.set.call(this, e)
                        },
                        setup_: function (e) {
                            function t() {
                                s["default"].get().then(function (t) {
                                    i.summaries = t, i.accounts = i.summaries.all(), e()
                                }, function (e) {
                                    i.emit("error", e)
                                })
                            }

                            var i = this;
                            gapi.analytics.auth.isAuthorized() ? t() : gapi.analytics.auth.on("signIn", t)
                        },
                        updateAccounts_: function () {
                            var e = this.get(), i = t(e), r = void 0, o = void 0, s = void 0;
                            if (!this.summaries.all().length) return this.emit("error", new Error('This user does not have any Google Analytics accounts. You can sign up at "www.google.com/analytics".'));
                            if (i) switch (i.prop) {
                                case"viewId":
                                    r = this.summaries.getProfile(i.value), o = this.summaries.getAccountByProfileId(i.value), s = this.summaries.getWebPropertyByProfileId(i.value);
                                    break;
                                case"propertyId":
                                    s = this.summaries.getWebProperty(i.value), o = this.summaries.getAccountByWebPropertyId(i.value), r = s && s.views && s.views[0];
                                    break;
                                case"accountId":
                                    o = this.summaries.getAccount(i.value), s = o && o.properties && o.properties[0], r = s && s.views && s.views[0]
                            } else o = this.accounts[0], s = o && o.properties && o.properties[0], r = s && s.views && s.views[0];
                            o || s || r ? (o != this.account || s != this.property || r != this.view) && (this.changed_ = {
                                account: o && o != this.account,
                                property: s && s != this.property,
                                view: r && r != this.view
                            }, this.account = o, this.properties = o.properties, this.property = s, this.views = s && s.views, this.view = r, this.ids = r && "ga:" + r.id) : this.emit("error", new Error("This user does not have access to " + i.prop.slice(0, -2) + " : " + i.value))
                        },
                        render_: function () {
                            var t = this.get();
                            this.container = "string" == typeof t.container ? document.getElementById(t.container) : t.container, this.container.innerHTML = t.template || this.template;
                            var i = this.container.querySelectorAll("select"), r = this.accounts,
                                o = this.properties || [{name: "(Empty)", id: ""}],
                                s = this.views || [{name: "(Empty)", id: ""}];
                            e(i[0], r, this.account.id), e(i[1], o, this.property && this.property.id), e(i[2], s, this.view && this.view.id), i[0].onchange = this.onUserSelect_.bind(this, i[0], "accountId"), i[1].onchange = this.onUserSelect_.bind(this, i[1], "propertyId"), i[2].onchange = this.onUserSelect_.bind(this, i[2], "viewId")
                        },
                        onChange_: function () {
                            var e = {
                                account: this.account,
                                property: this.property,
                                view: this.view,
                                ids: this.view && "ga:" + this.view.id
                            };
                            this.changed_ && (this.changed_.account && this.emit("accountChange", e), this.changed_.property && this.emit("propertyChange", e), this.changed_.view && (this.emit("viewChange", e), this.emit("idsChange", e), this.emit("change", e.ids))), this.changed_ = null
                        },
                        onUserSelect_: function (e, t) {
                            var i = {};
                            i[t] = e.value, this.set(i), this.execute()
                        },
                        template: '<div class="ViewSelector2">  <div class="ViewSelector2-item">    <label>Account</label>    <select class="FormField"></select>  </div>  <div class="ViewSelector2-item">    <label>Property</label>    <select class="FormField"></select>  </div>  <div class="ViewSelector2-item">    <label>View</label>    <select class="FormField"></select>  </div></div>'
                    })
                })
            }, function (e, t, i) {
                function r() {
                    var e = gapi.client.request({path: n}).then(function (e) {
                        return e
                    });
                    return new e.constructor(function (t, i) {
                        var r = [];
                        e.then(function o(e) {
                            var c = e.result;
                            c.items ? r = r.concat(c.items) : i(new Error("You do not have any Google Analytics accounts. Go to http://google.com/analytics to sign up.")), c.startIndex + c.itemsPerPage <= c.totalResults ? gapi.client.request({
                                path: n,
                                params: {"start-index": c.startIndex + c.itemsPerPage}
                            }).then(o) : t(new s(r))
                        }).then(null, i)
                    })
                }

                var o, s = i(2), n = "/analytics/v3/management/accountSummaries";
                e.exports = {
                    get: function (e) {
                        return e && (o = null), o || (o = r())
                    }
                }
            }, function (e, t) {
                function i(e) {
                    this.accounts_ = e, this.webProperties_ = [], this.profiles_ = [], this.accountsById_ = {}, this.webPropertiesById_ = this.propertiesById_ = {}, this.profilesById_ = this.viewsById_ = {};
                    for (var t, i = 0; t = this.accounts_[i]; i++) if (this.accountsById_[t.id] = {self: t}, t.webProperties) {
                        r(t, "webProperties", "properties");
                        for (var o, s = 0; o = t.webProperties[s]; s++) if (this.webProperties_.push(o), this.webPropertiesById_[o.id] = {
                            self: o,
                            parent: t
                        }, o.profiles) {
                            r(o, "profiles", "views");
                            for (var n, c = 0; n = o.profiles[c]; c++) this.profiles_.push(n), this.profilesById_[n.id] = {
                                self: n,
                                parent: o,
                                grandParent: t
                            }
                        }
                    }
                }

                function r(e, t, i) {
                    Object.defineProperty ? Object.defineProperty(e, i, {
                        get: function () {
                            return e[t]
                        }
                    }) : e[i] = e[t]
                }

                i.prototype.all = function () {
                    return this.accounts_
                }, r(i.prototype, "all", "allAccounts"), i.prototype.allWebProperties = function () {
                    return this.webProperties_
                }, r(i.prototype, "allWebProperties", "allProperties"), i.prototype.allProfiles = function () {
                    return this.profiles_
                }, r(i.prototype, "allProfiles", "allViews"), i.prototype.get = function (e) {
                    if (!!e.accountId + !!e.webPropertyId + !!e.propertyId + !!e.profileId + !!e.viewId > 1) throw new Error('get() only accepts an object with a single property: either "accountId", "webPropertyId", "propertyId", "profileId" or "viewId"');
                    return this.getProfile(e.profileId || e.viewId) || this.getWebProperty(e.webPropertyId || e.propertyId) || this.getAccount(e.accountId)
                }, i.prototype.getAccount = function (e) {
                    return this.accountsById_[e] && this.accountsById_[e].self
                }, i.prototype.getWebProperty = function (e) {
                    return this.webPropertiesById_[e] && this.webPropertiesById_[e].self
                }, r(i.prototype, "getWebProperty", "getProperty"), i.prototype.getProfile = function (e) {
                    return this.profilesById_[e] && this.profilesById_[e].self
                }, r(i.prototype, "getProfile", "getView"), i.prototype.getAccountByProfileId = function (e) {
                    return this.profilesById_[e] && this.profilesById_[e].grandParent
                }, r(i.prototype, "getAccountByProfileId", "getAccountByViewId"), i.prototype.getWebPropertyByProfileId = function (e) {
                    return this.profilesById_[e] && this.profilesById_[e].parent
                }, r(i.prototype, "getWebPropertyByProfileId", "getPropertyByViewId"), i.prototype.getAccountByWebPropertyId = function (e) {
                    return this.webPropertiesById_[e] && this.webPropertiesById_[e].parent
                }, r(i.prototype, "getAccountByWebPropertyId", "getAccountByPropertyId"), e.exports = i
            }]);
            // DateRange Selector JS
            !function (t) {
                function e(n) {
                    if (a[n]) return a[n].exports;
                    var i = a[n] = {exports: {}, id: n, loaded: !1};
                    return t[n].call(i.exports, i, i.exports, e), i.loaded = !0, i.exports
                }

                var a = {};
                return e.m = t, e.c = a, e.p = "", e(0)
            }([function (t, e) {
                "use strict";
                gapi.analytics.ready(function () {
                    function t(t) {
                        if (n.test(t)) return t;
                        var i = a.exec(t);
                        if (i) return e(+i[1]);
                        if ("today" == t) return e(0);
                        if ("yesterday" == t) return e(1);
                        throw new Error("Cannot convert date " + t)
                    }

                    function e(t) {
                        var e = new Date;
                        e.setDate(e.getDate() - t);
                        var a = String(e.getMonth() + 1);
                        a = 1 == a.length ? "0" + a : a;
                        var n = String(e.getDate());
                        return n = 1 == n.length ? "0" + n : n, e.getFullYear() + "-" + a + "-" + n
                    }

                    var a = /(\d+)daysAgo/, n = /\d{4}\-\d{2}\-\d{2}/;
                    gapi.analytics.createComponent("DateRangeSelector", {
                        execute: function () {
                            var e = this.get();
                            e["start-date"] = e["start-date"] || "7daysAgo", e["end-date"] = e["end-date"] || "yesterday", this.container = "string" == typeof e.container ? document.getElementById(e.container) : e.container, e.template && (this.template = e.template), this.container.innerHTML = this.template;
                            var a = this.container.querySelectorAll("input");
                            return this.startDateInput = a[0], this.startDateInput.value = t(e["start-date"]), this.endDateInput = a[1], this.endDateInput.value = t(e["end-date"]), this.setValues(), this.setMinMax(), this.container.onchange = this.onChange.bind(this), this
                        },
                        onChange: function () {
                            this.setValues(), this.setMinMax(), this.emit("change", {
                                "start-date": this["start-date"],
                                "end-date": this["end-date"]
                            })
                        },
                        setValues: function () {
                            this["start-date"] = this.startDateInput.value, this["end-date"] = this.endDateInput.value
                        },
                        setMinMax: function () {
                            this.startDateInput.max = this.endDateInput.value, this.endDateInput.min = this.startDateInput.value
                        },
                        template: '<div class="DateRangeSelector">  <div class="DateRangeSelector-item">    <label>Start Date</label>     <input type="date">  </div>  <div class="DateRangeSelector-item">    <label>End Date</label>     <input type="date">  </div></div>'
                    })
                })
            }]);
            // Active Users JS
            !function (t) {
                function i(s) {
                    if (e[s]) return e[s].exports;
                    var n = e[s] = {exports: {}, id: s, loaded: !1};
                    return t[s].call(n.exports, n, n.exports, i), n.loaded = !0, n.exports
                }

                var e = {};
                return i.m = t, i.c = e, i.p = "", i(0)
            }([function (t, i) {
                "use strict";
                gapi.analytics.ready(function () {
                    gapi.analytics.createComponent("ActiveUsers", {
                        initialize: function () {
                            this.activeUsers = 0, gapi.analytics.auth.once("signOut", this.handleSignOut_.bind(this))
                        }, execute: function () {
                            this.polling_ && this.stop(), this.render_(), gapi.analytics.auth.isAuthorized() ? this.pollActiveUsers_() : gapi.analytics.auth.once("signIn", this.pollActiveUsers_.bind(this))
                        }, stop: function () {
                            clearTimeout(this.timeout_), this.polling_ = !1, this.emit("stop", {activeUsers: this.activeUsers})
                        }, render_: function () {
                            var t = this.get();
                            this.container = "string" == typeof t.container ? document.getElementById(t.container) : t.container, this.container.innerHTML = t.template || this.template, this.container.querySelector("b").innerHTML = this.activeUsers
                        }, pollActiveUsers_: function () {
                            var t = this.get(), i = 1e3 * (t.pollingInterval || 5);
                            if (isNaN(i) || 5e3 > i) throw new Error("Frequency must be 5 seconds or more.");
                            this.polling_ = !0, gapi.client.analytics.data.realtime.get({
                                ids: t.ids,
                                metrics: "rt:activeUsers"
                            }).then(function (t) {
                                var e = t.result, s = e.totalResults ? +e.rows[0][0] : 0, n = this.activeUsers;
                                this.emit("success", {activeUsers: this.activeUsers}), s != n && (this.activeUsers = s, this.onChange_(s - n)), 1 == this.polling_ && (this.timeout_ = setTimeout(this.pollActiveUsers_.bind(this), i))
                            }.bind(this))
                        }, onChange_: function (t) {
                            var i = this.container.querySelector("b");
                            i && (i.innerHTML = this.activeUsers), this.emit("change", {
                                activeUsers: this.activeUsers,
                                delta: t
                            }), t > 0 ? this.emit("increase", {
                                activeUsers: this.activeUsers,
                                delta: t
                            }) : this.emit("decrease", {activeUsers: this.activeUsers, delta: t})
                        }, handleSignOut_: function () {
                            this.stop(), gapi.analytics.auth.once("signIn", this.handleSignIn_.bind(this))
                        }, handleSignIn_: function () {
                            this.pollActiveUsers_(), gapi.analytics.auth.once("signOut", this.handleSignOut_.bind(this))
                        }, template: '<div class="ActiveUsers">Active Users: <b class="ActiveUsers-value"></b></div>'
                    })
                })
            }]);
        </script>

        <script>
            // == NOTE ==
            // This code uses ES6 promises. If you want to use this code in a browser
            // that doesn't supporting promises natively, you'll have to include a polyfill.

            gapi.analytics.ready(function () {

                /**
                 * Authorize the user immediately if the user has already granted access.
                 * If no access has been created, render an authorize button inside the
                 * element with the ID "embed-api-auth-container".
                 */
                gapi.analytics.auth.authorize({
                    container: 'embed-api-auth-container',
                    clientid: '{{ $google_analytics_client_id }}'
                });


                /**
                 * Create a new ActiveUsers instance to be rendered inside of an
                 * element with the id "active-users-container" and poll for changes every
                 * five seconds.
                 */
                var activeUsers = new gapi.analytics.ext.ActiveUsers({
                    container: 'active-users-container',
                    pollingInterval: 5
                });


                /**
                 * Add CSS animation to visually show the when users come and go.
                 */
                activeUsers.once('success', function () {
                    var element = this.container.firstChild;
                    var timeout;

                    document.getElementById('embed-api-auth-container').style.display = 'none';
                    document.getElementById('analytics-dashboard').style.display = 'block';

                    this.on('change', function (data) {
                        var element = this.container.firstChild;
                        var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
                        element.className += (' ' + animationClass);

                        clearTimeout(timeout);
                        timeout = setTimeout(function () {
                            element.className =
                                element.className.replace(/ is-(increasing|decreasing)/g, '');
                        }, 3000);
                    });
                });


                /**
                 * Create a new ViewSelector2 instance to be rendered inside of an
                 * element with the id "view-selector-container".
                 */
                var viewSelector = new gapi.analytics.ext.ViewSelector2({
                    container: 'view-selector-container',
                    propertyId: '{{ Voyager::setting("site.google_analytics_tracking_id")  }}'
                })
                    .execute();


                /**
                 * Update the activeUsers component, the Chartjs charts, and the dashboard
                 * title whenever the user changes the view.
                 */
                viewSelector.on('viewChange', function (data) {
                    var title = document.getElementById('view-name');
                    if (title) {
                        title.innerHTML = data.property.name + ' (' + data.view.name + ')';
                    }

                    // Start tracking active users for this view.
                    activeUsers.set(data).execute();

                    // Render all the of charts for this view.
                    renderWeekOverWeekChart(data.ids);
                    renderYearOverYearChart(data.ids);
                    renderTopBrowsersChart(data.ids);
                    renderTopCountriesChart(data.ids);
                });


                /**
                 * Draw the a chart.js line chart with data from the specified view that
                 * overlays session data for the current week over session data for the
                 * previous week.
                 */
                function renderWeekOverWeekChart(ids) {

                    // Adjust `now` to experiment with different days, for testing only...
                    var now = moment(); // .subtract(3, 'day');

                    var thisWeek = query({
                        'ids': ids,
                        'dimensions': 'ga:date,ga:nthDay',
                        'metrics': 'ga:users',
                        'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD'),
                        'end-date': moment(now).format('YYYY-MM-DD')
                    });

                    var lastWeek = query({
                        'ids': ids,
                        'dimensions': 'ga:date,ga:nthDay',
                        'metrics': 'ga:users',
                        'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week')
                            .format('YYYY-MM-DD'),
                        'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week')
                            .format('YYYY-MM-DD')
                    });

                    Promise.all([thisWeek, lastWeek]).then(function (results) {

                        var data1 = results[0].rows.map(function (row) {
                            return +row[2];
                        });
                        var data2 = results[1].rows.map(function (row) {
                            return +row[2];
                        });
                        var labels = results[1].rows.map(function (row) {
                            return +row[0];
                        });

                        labels = labels.map(function (label) {
                            return moment(label, 'YYYYMMDD').format('ddd');
                        });

                        var data = {
                            labels: labels,
                            datasets: [
                                {
                                    label: '{{ __('voyager::date.last_week') }}',
                                    fillColor: 'rgba(220,220,220,0.5)',
                                    strokeColor: 'rgba(220,220,220,1)',
                                    pointColor: 'rgba(220,220,220,1)',
                                    pointStrokeColor: '#fff',
                                    data: data2
                                },
                                {
                                    label: '{{ __('voyager::date.this_week') }}',
                                    fillColor: 'rgba(151,187,205,0.5)',
                                    strokeColor: 'rgba(151,187,205,1)',
                                    pointColor: 'rgba(151,187,205,1)',
                                    pointStrokeColor: '#fff',
                                    data: data1
                                }
                            ]
                        };

                        new Chart(makeCanvas('chart-1-container')).Line(data);
                        generateLegend('legend-1-container', data.datasets);
                    });
                }


                /**
                 * Draw the a chart.js bar chart with data from the specified view that
                 * overlays session data for the current year over session data for the
                 * previous year, grouped by month.
                 */
                function renderYearOverYearChart(ids) {

                    // Adjust `now` to experiment with different days, for testing only...
                    var now = moment(); // .subtract(3, 'day');

                    var thisYear = query({
                        'ids': ids,
                        'dimensions': 'ga:month,ga:nthMonth',
                        'metrics': 'ga:users',
                        'start-date': moment(now).date(1).month(0).format('YYYY-MM-DD'),
                        'end-date': moment(now).format('YYYY-MM-DD')
                    });

                    var lastYear = query({
                        'ids': ids,
                        'dimensions': 'ga:month,ga:nthMonth',
                        'metrics': 'ga:users',
                        'start-date': moment(now).subtract(1, 'year').date(1).month(0)
                            .format('YYYY-MM-DD'),
                        'end-date': moment(now).date(1).month(0).subtract(1, 'day')
                            .format('YYYY-MM-DD')
                    });

                    Promise.all([thisYear, lastYear]).then(function (results) {
                        var data1 = results[0].rows.map(function (row) {
                            return +row[2];
                        });
                        var data2 = results[1].rows.map(function (row) {
                            return +row[2];
                        });
                        var labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                        // Ensure the data arrays are at least as long as the labels array.
                        // Chart.js bar charts don't (yet) accept sparse datasets.
                        for (var i = 0, len = labels.length; i < len; i++) {
                            if (data1[i] === undefined) data1[i] = null;
                            if (data2[i] === undefined) data2[i] = null;
                        }

                        var data = {
                            labels: labels,
                            datasets: [
                                {
                                    label: '{{ __('voyager::date.last_year') }}',
                                    fillColor: 'rgba(220,220,220,0.5)',
                                    strokeColor: 'rgba(220,220,220,1)',
                                    data: data2
                                },
                                {
                                    label: '{{ __('voyager::date.this_year') }}',
                                    fillColor: 'rgba(151,187,205,0.5)',
                                    strokeColor: 'rgba(151,187,205,1)',
                                    data: data1
                                }
                            ]
                        };

                        new Chart(makeCanvas('chart-2-container')).Bar(data);
                        generateLegend('legend-2-container', data.datasets);
                    })
                        .catch(function (err) {
                            console.error(err.stack);
                        });
                }


                /**
                 * Draw the a chart.js doughnut chart with data from the specified view that
                 * show the top 5 browsers over the past seven days.
                 */
                function renderTopBrowsersChart(ids) {

                    query({
                        'ids': ids,
                        'dimensions': 'ga:browser',
                        'metrics': 'ga:pageviews',
                        'sort': '-ga:pageviews',
                        'max-results': 5
                    })
                        .then(function (response) {

                            var data = [];
                            var colors = ['#4D5360', '#949FB1', '#D4CCC5', '#E2EAE9', '#F7464A'];

                            response.rows.forEach(function (row, i) {
                                data.push({value: +row[1], color: colors[i], label: row[0]});
                            });

                            new Chart(makeCanvas('chart-3-container')).Doughnut(data);
                            generateLegend('legend-3-container', data);
                        });
                }


                /**
                 * Draw the a chart.js doughnut chart with data from the specified view that
                 * compares sessions from mobile, desktop, and tablet over the past seven
                 * days.
                 */
                function renderTopCountriesChart(ids) {
                    query({
                        'ids': ids,
                        'dimensions': 'ga:country',
                        'metrics': 'ga:sessions',
                        'sort': '-ga:sessions',
                        'max-results': 5
                    })
                        .then(function (response) {

                            var data = [];
                            var colors = ['#4D5360', '#949FB1', '#D4CCC5', '#E2EAE9', '#F7464A'];

                            response.rows.forEach(function (row, i) {
                                data.push({
                                    label: row[0],
                                    value: +row[1],
                                    color: colors[i]
                                });
                            });

                            new Chart(makeCanvas('chart-4-container')).Doughnut(data);
                            generateLegend('legend-4-container', data);
                        });
                }


                /**
                 * Extend the Embed APIs `gapi.analytics.report.Data` component to
                 * return a promise the is fulfilled with the value returned by the API.
                 * @param {Object} params The request parameters.
                 * @return {Promise} A promise.
                 */
                function query(params) {
                    return new Promise(function (resolve, reject) {
                        var data = new gapi.analytics.report.Data({query: params});
                        data.once('success', function (response) {
                            resolve(response);
                        })
                            .once('error', function (response) {
                                reject(response);
                            })
                            .execute();
                    });
                }


                /**
                 * Create a new canvas inside the specified element. Set it to be the width
                 * and height of its container.
                 * @param {string} id The id attribute of the element to host the canvas.
                 * @return {RenderingContext} The 2D canvas context.
                 */
                function makeCanvas(id) {
                    var container = document.getElementById(id);
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext('2d');

                    container.innerHTML = '';
                    canvas.width = container.offsetWidth;
                    canvas.height = container.offsetHeight;
                    container.appendChild(canvas);

                    return ctx;
                }


                /**
                 * Create a visual legend inside the specified element based off of a
                 * Chart.js dataset.
                 * @param {string} id The id attribute of the element to host the legend.
                 * @param {Array.<Object>} items A list of labels and colors for the legend.
                 */
                function generateLegend(id, items) {
                    var legend = document.getElementById(id);
                    legend.innerHTML = items.map(function (item) {
                        var color = item.color || item.fillColor;
                        var label = item.label;
                        return '<li><i style="background:' + color + '"></i>' + label + '</li>';
                    }).join('');
                }


                // Set some global Chart.js defaults.
                Chart.defaults.global.animationSteps = 60;
                Chart.defaults.global.animationEasing = 'easeInOutQuart';
                Chart.defaults.global.responsive = true;
                Chart.defaults.global.maintainAspectRatio = false;

                // resize to redraw charts
                window.dispatchEvent(new Event('resize'));

            });
        </script>

    @endif

@stop
